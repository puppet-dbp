# dbp.pp - EDV-Beratung&Service Debian Best Practices
# Copyright (C) 2007 David Schmitt <david@schmitt.edv-bus.at>
# See LICENSE for the full license granted to you.
# 24 July 2010 -- James Richardson -- I am gutting everything
# in this file (hopefully with a comment of (#jmr). As I understand
# what this is trying to do I will add stuff back. The biggest issue
# I see right off is he reomoves some packages that I need (pppeo on
# my firewall for instance) and assumes that nagios and munin exist.
#
#jmr import "vim.pp"
	
class dbp {

	module_dir { "dbp": }

	# croak iff we are not running on Debian
	case $operatingsystem {
		'Debian': { }
		default: { fail("\$operatingsystem of ${fqdn} is not 'Debian' but '${operatingsystem}'.") }
	}

	# Save space here: vservers don't need old packages around, there will
	# be always network around to fix them
	$apt_clean = $vserver ? { 
		'guest' => 'always',
		default => 'auto',
	}

	#######################################################################
	#######################################################################

	include apt
#jmr	include vim
#jmr	include ssh::client
#jmr	include munin::client
#jmr	include dbp::syslog

	# package was renamed from sarge to etch
	# TODO: use conditional command support of providers to use grep-dctrl
	# for improved performance
	$dctrl = $lsbdistcodename ? { sarge => grep-dctrl, default => dctrl-tools }

	# default packages
	package {
		[
			# get daily updates on pending security upgrades
			apticron,
			apt-listchanges,
			# a few useful tools
			screen, mmv, deborphan, less, iproute, bzip2,
			$dctrl, net-tools, man-db,
                        # monitor logs
                        logwatch,
			# always handy
			sudo, pwgen,
			# enable good error reporting
			reportbug,
			# required for current facter
			pciutils,
                        # needed to manage my dotfils
                        rake
		]:
			ensure => installed;
		# has Installed-Size: 20, compared to the 900+ of -i18n
		debconf-english:
			ensure => installed, before => Package[debconf-i18n];
		debconf-i18n:
			ensure => purged;
#jmr		[
#jmr			# things installed by default which are not used:
#jmr			lpr, ppp, pppconfig, pppoe, pppoeconf,
#jmr			tasksel, tasksel-data, installation-report,
#jmr			dmidecode, laptop-detect, whiptail, info,
#jmr			# legacy or standard packages that really shouldn't be installed
#jmr			pidentd, pump, nfs-common, nfs-kernel-server, nagios-nrpe-server
#jmr		]:
#jmr			ensure => purged;
	}

	#######################################################################
	#######################################################################

	package {
		# it is in my interest to provide finegrained infos to
		# debian, to vote on QA and CD work
		popularity-contest:
			ensure => installed,
			before => Replace[fix_popularity_contest],
	}
	replace { fix_popularity_contest:
		file => "/etc/popularity-contest.conf",
		pattern => '^PARTICIPATE="no"',
		replacement => 'PARTICIPATE="yes"',
	}

	#######################################################################
	#######################################################################

	# As recommended by /etc/bash.bashrc, source this file in /etc/profile,
	# so that these settings / commands are available in login shells too.
	line { source_bashrc:
		file => "/etc/profile",
		line => "source /etc/bash.bashrc",
		ensure => present,
	}

	case $lsbdistcodename {
		"sarge", "etch": { }
		default: {
			package { "bash-completion": ensure => installed }
		}
	}

	#######################################################################
	#######################################################################

	# various config files
	file {
		"/etc/apt/listchanges.conf":
			mode => 0644, owner => root, group => root,
			before => [ File[apt_config], Package[apt-listchanges] ],
			source => "puppet:///modules/dbp/apt-listchanges.conf";
		"/etc/bash.bashrc":
			mode => 0644, owner => root, group => root,
			source => "puppet:///modules/dbp/bash.bashrc";
		"/etc/localtime":
			mode => 0644, owner => root, group => root,
			source => "/usr/share/zoneinfo/America/New_York";
		"/etc/timezone":
			mode => 0644, owner => root, group => root,
			content => "US/Eastern\n";
#jmr		# all my hosts are defined manually in this file
#jmr		# TODO: replace this with a more automated approach
#jmr		"/etc/hosts":
#jmr			mode => 0644, owner => root, group => root,
#jmr			source => "puppet:///modules/dbp/hosts";
#jmr		# lenny doesn't ship this file, but pam_env complains
#jmr		"/etc/environment":
#jmr			mode => 0644, owner => root, group => root,
#jmr			ensure => present;
	}
	
	#######################################################################
	#######################################################################

	# as far as I can tell, this is enables UTF-8
	package { locales: ensure => installed }
	# setup locales and generate them
	file {
		"/etc/default/locale":
			mode => 0644, owner => root, group => root,
			source => "puppet:///modules/dbp/default_locale";
		"/etc/locale.gen":
			mode => 0644, owner => root, group => root,
			source => "puppet:///modules/dbp/locale.gen";
	}
	# setup all the needed datastructures
	exec { "/usr/sbin/locale-gen":
		refreshonly => true,
		require => Package[locales],
		subscribe => File["/etc/locale.gen"]
	}

	#######################################################################
	#######################################################################

	# keep puppet up to date

	file {
		"/etc/puppet/puppet.conf":
			source => "puppet:///modules/dbp/puppet.conf",
			mode => 0644, owner => root, group => root;
		# legacy config file
		"/etc/puppet/puppetd.conf": ensure => absent;
#jmr		"${module_dir_path}/dbp/puppet_current.deb":
#jmr			source => "puppet:///modules/dbp/puppet_current.deb",
#jmr			mode => 0644, owner => root, group => root,
#jmr			notify => Package[puppet];
#jmr		"${module_dir_path}/dbp/puppet-common_current.deb":
#jmr			source => "puppet:///modules/dbp/puppet-common_current.deb",
#jmr			mode => 0644, owner => root, group => root,
#jmr			notify => Package[puppet-common];
#jmr		"${module_dir_path}/dbp/facter_current.deb":
#jmr			source => "puppet:///modules/dbp/facter_current.deb",
#jmr			mode => 0644, owner => root, group => root,
#jmr			notify => Package[facter];
	}

        ## ruby. squeeze and later libruby pulls in stuff.
        case $lsbdistid {
          Debian: {
            case $lsbdistcodename {
                 etch, sarge, lenny: {
                        package { [ "libxmlrpc-ruby", "libopenssl-ruby" ]: ensure => installed; }
                        }
                 lenny: {
                    package { "libruby": ensure => installed; }
                    }
            }
          }
        }
#
	package {
		[ "ruby", "libshadow-ruby1.8", "adduser",
		  "lsb-base", "lsb-release", "host"
		]:
			ensure => installed;
#jmr		"facter": 
#jmr			provider => dpkg,
#jmr			source => "${module_dir_path}/dbp/facter_current.deb",
#jmr			ensure => latest,
#jmr			require => [ Package["ruby"], Package["host"], Package['pciutils'] ];
#jmr		"puppet-common": 
#jmr			provider => dpkg,
#jmr			source => "${module_dir_path}/dbp/puppet-common_current.deb",
#jmr			ensure => latest,
#jmr			require => [ Package["ruby"], Package["libxmlrpc-ruby"],
#jmr				Package["libopenssl-ruby"], Package["libshadow-ruby1.8"],
#jmr				Package["adduser"], Package["facter"], Package["lsb-base"],
#jmr				Package["lsb-release"] ];
#jmr		"puppet": 
#jmr			provider => dpkg,
#jmr			source => "${module_dir_path}/dbp/puppet_current.deb",
#jmr			ensure => latest,
#jmr			require => Package["puppet-common"];
	}

	# Puppet and VServers have two bad interactions:
	# * Running multiple puppetds in different VServers leads to very high memory usage
	# * booting a host causes all puppetds to hit the master at the same time
	# to work around both problems, the following resources disable the
	# allways running puppetd and instead use cron and vserver-exec to run
	# puppetd sequentially once per hour in all vservers.

	# disable the service
	service { puppet:
		enable => false,
		pattern => puppetd,
	}
	# install the cronjob to run puppet, but not in vserver guests
	file {
		"/etc/cron.hourly/puppet":
			ensure => $vserver ? { 'guest' => absent, default => present, },
			source => "puppet:///modules/dbp/cron.hourly.puppet",
			mode => 0755, owner => root, group => root;
	}

#jmr	puppet::munin::resources { $fqdn: }

	#######################################################################
	#######################################################################
#jmr
#jmr	# include default user
#jmr	case $no_default_user { 
#jmr		'true': { }
#jmr		default: { include david }
#jmr	}
#jmr
	#######################################################################
	#######################################################################

#jmr	# include additional stuff when running under various virtualisations
#jmr	case $vserver { 
#jmr		'host': {
#jmr			include vserver::host
#jmr		}
#jmr	}
#jmr
#jmr	case $virtual { 
#jmr		'xen0': {
#jmr			include xen::domain
#jmr		}
#jmr		'xenu': {
#jmr			include xen::domain
#jmr		}
#jmr	}
#jmr
#jmr	#######################################################################
#jmr	#######################################################################
#jmr
#jmr	# best practises for vserver: remove additional packages, disable many
#jmr	# unneeded services
#jmr	case $vserver { 
#jmr		'guest': {
#jmr			package {
#jmr				# These packages are never needed in the context of a VServer
#jmr				[ module-init-tools, modutils, dhcp-client, dhcp3-client,
#jmr				dhcp3-common, ntp, ntp-server, ntpdate, smartmontools, acpi ]:
#jmr					ensure => purged;
#jmr				# One might argue about these, but usually they are not needed either
#jmr				[ manpages, iptables, traceroute, perl-doc, dlocate]:
#jmr					ensure => purged;
#jmr				# These are suid or can be used to access the network, thus we are
#jmr				# safer without them
#jmr				[ netcat, strace, lsof, tcpdump ]:
#jmr					ensure => purged;
#jmr				# apache needs this!?!
#jmr				# w3m: ensure => purged
#jmr			}
#jmr			# more stuff that is not needed in a vserver
#jmr			service {
#jmr				[ "bootlogd", "bootmisc.sh", "checkfs.sh", "checkroot.sh",
#jmr				"console-screen.sh", "halt", "hostname.sh", "hwclock.sh",
#jmr				"hwclockfirst.sh", "ifupdown", "ifupdown-clean", "keymap.sh",
#jmr				"klogd", "makedev", "modutils", "mountall.sh", "mountnfs.sh",
#jmr				"mountvirtfs", "networking", "procps.sh", "reboot", "rmnologin",
#jmr				"single", "stop-bootlogd", "umountfs", "umountnfs.sh", "urandom",
#jmr				"umountroot" ]:
#jmr					enable => false
#jmr			}
#jmr
#jmr			# provide dummy dependency for collected files from ntp
#jmr			include ntp::none
#jmr
#jmr		}
#jmr		'': {
#jmr			err("${fqdn}: Unknown vserver status, delaying specific parts")
#jmr			include ntp::none
#jmr		}
#jmr		# some packages which are only needed outside of a guest
#jmr		default: {
#jmr			package {
#jmr				[dlocate, w3m, wget, smartmontools, acpi,
#jmr				tcpdump ]:
#jmr					ensure => installed
#jmr			}
#jmr
#jmr			case $lsbdistid {
#jmr				Debian: {
#jmr					case $lsbdistcodename {
#jmr						etch, sarge: { 
#jmr							package { "dhcp-client": ensure => installed; }
#jmr						}
#jmr						lenny: {
#jmr							package { "dhcp3-client": ensure => installed; }
#jmr						}
#jmr					}
#jmr				}
#jmr			}
#jmr			
#jmr			include ntp
#jmr			include backuppc::client
#jmr		}
#jmr	}
#jmr
#jmr	#######################################################################
#jmr	#######################################################################
#jmr
#jmr	case $mta {
#jmr		'ssmtp': { include ssmtp }
#jmr		'exim4': { include exim4 }
#jmr		'': { debug ("${fqdn}: no MTA specified, leaving as is") }
#jmr		default: { err ("${fqdn}: unknown MTA '${mta}'") }
#jmr	}
#jmr
#jmr	#######################################################################
#jmr	#######################################################################

	# remove all local bucketed files after a month.
	tidy { '/var/lib/puppet/clientbucket':
		backup => false,
		recurse => true,
		rmdirs => true,
		type => mtime,
		age => '4w',
	}

	#######################################################################
	#######################################################################

#jmr	case $manage_dns {
#jmr		'': {
#jmr			# Export the own hostname into dns
#jmr			bind::a2 {
#jmr				"${fqdn}_${ipaddress}":
#jmr					rrname => $hostname,
#jmr					domain => $domain,
#jmr					ip => $ipaddress,
#jmr			}
#jmr		}
#jmr		'manual': {}
#jmr	}

	#######################################################################
	#######################################################################

#jmr	config_file {
#jmr		"/etc/updatedb.conf":
#jmr			source => [ "puppet:///modules/dbp/updatedb/${lsbdistcodename}.conf" ]
#jmr	}

}
