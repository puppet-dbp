# vim.pp - setup vim to my liking
# Copyright (C) 2007 David Schmitt <david@schmitt.edv-bus.at>
# See LICENSE for the full license granted to you.
	
class vim {

	package {
		vim: ensure => installed;
		[ nvi, vim-tiny ]: ensure => absent;
	}

	file { "/etc/vim/vimrc":
		source => [ "puppet:///modules/dbp/vimrc.$lsbdistcodename", "puppet:///modules/dbp/vimrc" ],
		mode => 0664, owner => root, group => root,
	}

}


